import { Grid, Stack, Title, Avatar, Text, Box, Group, ActionIcon } from "@mantine/core";
import { useQuery } from "@tanstack/react-query";
import { EyeOff, Eye } from 'tabler-icons-react';
import { useState } from "react";
import { separateNumber, upperCaseFisrtLetter } from "../../utils";
import { Fetch } from "../../services";
import { useAppSelector } from '../../redux/hooks';

function Dashboard() {
  const queryBalance = useQuery(['dashboard-balance'], Fetch.getBalance);
  const [isCreditShow, setIsCreditShow] = useState(false);
  const currentUser = useAppSelector((state) => state.user);
  
  return (
    <Grid>
      <Grid.Col span={6}>
        <Stack spacing={5}>
          <Avatar w={70} h={70} src={currentUser?.profile_image} radius='xl'  />
          <Text color="dimmed" size="sm" >
            Selamat datang,    
          </Text>
          <Title
            mb={28}
            sx={(theme) => ({
              fontFamily: `Greycliff CF, ${theme.fontFamily}`,
              fontSize: 20,
              fontWeight: 900,
              lineHeight: 1.1,
            })}
          >
            {upperCaseFisrtLetter(currentUser?.first_name ?? '')} {' '}
            {upperCaseFisrtLetter(currentUser?.last_name ?? '')}
          </Title>
        </Stack>
      </Grid.Col>
      <Grid.Col span={6}>
        <Box
          sx={{
            height: '140px',
            backgroundColor:'red',
            color: 'white',
            borderRadius: '15px',
            padding: '20px'
          }}
        >
          <Stack >
            <Text size={12} weight={600}>
                Saldo anda
            </Text>
            <Text size={24} weight={700}>
                Rp { isCreditShow ? separateNumber(queryBalance.data?.data?.balance) : '* * * * * *' }
            </Text>
            <Group spacing={1}>
              <Text size={11} weight={600}>
                { !isCreditShow ? 'Lihat'  : 'Tutup' } saldo 
              </Text>
              <ActionIcon variant="transparent" onClick={() => setIsCreditShow(!isCreditShow)}>{ isCreditShow ? <Eye color="white" size="1rem" /> : <EyeOff color="white" size="1rem" />}</ActionIcon>
            </Group>
          </Stack>
        </Box>
      </Grid.Col>
    </Grid>
  )
}

export default Dashboard;
