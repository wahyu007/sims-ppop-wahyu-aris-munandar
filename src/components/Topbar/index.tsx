import {
  Box,
  Burger,
  Button,
  Container,
  createStyles,
  Group,
  Header,
  Menu,
  Drawer,
  NavLink,
} from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import React from 'react';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { Fetch } from '../../services';
import { useAppDispatch } from '../../redux/hooks';
import { setUser  } from '../../redux/userReducer';

const useStyles = createStyles((theme) => ({
  inner: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 56,
    marginLeft: 55,
    marginRight: 30,

    [theme.fn.smallerThan('lg')]: {
      justifyContent: 'space-between',
    },
  },

  links: {
    width: 825,
    justifyContent: 'flex-end',

    [theme.fn.smallerThan('lg')]: {
      display: 'none',
    },
  },

  social: {
    width: 260,

    [theme.fn.smallerThan('sm')]: {
      width: 'auto',
      marginLeft: 'auto',
    },
  },

  burger: {
    marginRight: theme.spacing.md,

    [theme.fn.largerThan('lg')]: {
      display: 'none',
    },
  },

  link: {
    display: 'block',
    lineHeight: 1,
    padding: '8px 12px',
    borderRadius: theme.radius.sm,
    textDecoration: 'none',
    color:
      theme.colorScheme === 'dark'
        ? theme.colors.dark[0]
        : theme.colors.gray[7],
    fontSize: theme.fontSizes.sm,
    fontWeight: 800,

    '&:hover': {
      backgroundColor:
        theme.colorScheme === 'dark'
          ? theme.colors.dark[6]
          : theme.colors.gray[0],
    },
  },

  linkActive: {
    '&, &:hover': {
      backgroundColor: theme.fn.variant({
        variant: 'light',
        color: theme.primaryColor,
      }).background,
      color: theme.fn.variant({ variant: 'light', color: theme.primaryColor })
        .color,
    },
  },
}));

const links = [
  { label: 'Top Up', link: '/topup' },
  { label: 'Transaksi', link: '/transaksi' },
  { label: 'Akun', link: '/akun' }
];

function Topbar() {
  const navigate = useNavigate();
  const location = useLocation();
  const dispatch = useAppDispatch();
  const [opened, { toggle }] = useDisclosure(false);
  const { classes } = useStyles();

  function onActive(link: any) {
    if (location.pathname === link.link) return true;
    for (let i = 0; i < link?.children?.length; i += 1) {
      if (location.pathname === link.children[i].link) return true;
    }
    return false
  }

  useQuery(['dashboard-profile'], Fetch.getloggedIn, {
    refetchIntervalInBackground: false,
    onSuccess: (data) => {
      dispatch(setUser(data.data));
    }
  });

  const items2 = links.map((link) => (
    <Menu trigger="hover" key={link.label} shadow="md" width={200}>
      <Menu.Target>
        <Button
          variant={ onActive(link) ? 'filled' : 'subtle'}
          color='dark'
          onClick={(event) => {
            event.preventDefault();
            if (link.link) navigate(link.link);
          }}
        >{link.label}</Button>
      </Menu.Target>
    </Menu>
  ))

  const navItems = links.map((link) => (
    <NavLink
      key={link.label}
      label={link.label}
      onClick={(event) => {
        event.preventDefault();
        if (link.link) {
          navigate(link.link);
          toggle();
        }
      }}
      active={onActive(link)}
    />
  ));

  return (
    <Box mih="100vh">
      <Header height={56}>
        <Container className={classes.inner} fluid>
          <Burger
            opened={opened}
            onClick={toggle}
            size="sm"
            className={classes.burger}
          />

          <Group
            w={260} sx={{
              '&:hover' : {
                cursor: 'grab'
              }
            }}
            onClick={(event) => {
              event.preventDefault();
              navigate('/');
            } }
          >
            <img
              src="logo.png"
              alt="logo"
              style={{ width: 30 }}
            />
            <span>SIMS PPOB</span>
          </Group>

          <Group className={classes.links} spacing={3}>
            { items2 }
          </Group>

          <Drawer
            opened={opened}
            onClose={toggle}
            title={
              <Group style={{ padding: '10px' }} w={260}>
                <img
                  src="logo.png"
                  alt="logo"
                  style={{ width: 30 }}
                />
                <span>SIMS PPOB</span>
              </Group>
            }
          >
            { navItems }
          </Drawer>
        </Container>
      </Header>
      <Outlet />
    </Box>
  );
}

export default Topbar;
