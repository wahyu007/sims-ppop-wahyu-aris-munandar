import Dashboard from './Dashboard';
import Login from './Login';
import Register from './Register';
import Transaction from './Transaksi';
import TopUp from './TopUp';
import Akun from './Akun';

export {
  Dashboard,
  Login,
  Transaction,
  Register,
  TopUp,
  Akun
};
