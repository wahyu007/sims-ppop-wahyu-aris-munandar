/* eslint-disable react/no-unused-prop-types */
import {
  Container,
  Grid,
  Text,
  Stack,
  Title,
  TextInput,
  Button,
  SimpleGrid,
} from '@mantine/core';
import { useState } from 'react';
import { Moneybag } from 'tabler-icons-react';
import { openModal } from '@mantine/modals';
import { SaldoBox } from '../../components';
import ConfirmForm from './Form';
import { Fetch } from '../../services';

const nominals = [
  {
    name: 'Rp10.000',
    value: 10000
  },
  {
    name: 'Rp25.000',
    value: 25000
  },
  {
    name: 'Rp50.000',
    value: 50000
  },
  {
    name: 'Rp100.000',
    value: 100000
  },
  {
    name: 'Rp250.000',
    value: 250000
  },
  {
    name: 'Rp500.000',
    value: 500000
  },
]

function Dashboard() {
  const [value, setValue] = useState<string | number>('');

  const submitUser = async (nom: number) =>
    Fetch.postTopUp({
      top_up_amount: nom,
    })

  return (
    <Container my={52} mb={48} size="lg">
      <SaldoBox />
      <Grid>
        <Grid.Col>
          <Stack mt={30}>
            <Text color="dimmed" size="sm" >
              Silahkan masukan
            </Text>
            <Title
              mb={28}
              sx={(theme) => ({
                fontFamily: `Greycliff CF, ${theme.fontFamily}`,
                fontSize: 20,
                fontWeight: 900,
                lineHeight: 1.1,
              })}
            >
              Nominal Top Up
            </Title>
          </Stack> 
        </Grid.Col>
      </Grid>
      <Grid>
        <Grid.Col span={8}>
          <Stack>
            <TextInput
              icon={<Moneybag size="0.8rem" />}
              type='number'
              value={value}
              name="nominal"
              onChange={(val: React.ChangeEvent<HTMLInputElement>) => setValue(val.target.value)}
              placeholder='masukan nominal Top Up'
            />
            <Button
              disabled={Number(value) < 10000}
              onClick={() => {
                openModal({
                  size: "sm",
                  centered: true,
                  withCloseButton: false,
                  children: (
                    <ConfirmForm
                      data={value}
                      onFetch={submitUser}
                      queryKey={['dashboard-balance']}
                    />
                  ),
                })
              }}
            >Top Up</Button>
          </Stack>
        </Grid.Col>
        <Grid.Col span={4}>
          <SimpleGrid cols={3}>
            {nominals.map((button) => (
              <Button variant='outline' sx={{ borderColor: '#ddd' }} onClick={() => setValue(button.value)} color='dark' key={button.value}>
                {button.name}
              </Button>
            ))}
          </SimpleGrid>
        </Grid.Col>
      </Grid>
    </Container>
  );
}

export default Dashboard;
