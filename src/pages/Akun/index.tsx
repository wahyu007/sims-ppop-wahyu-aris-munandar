/* eslint-disable react/no-unused-prop-types */
import {
  Container,
  Grid,
  TextInput,
  Paper,
  Button,
  Box,
  Avatar,
  Text,
  Stack,
  FileButton,
  rem,
  ActionIcon
} from '@mantine/core';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useForm } from '@mantine/form';
import { At, User, Pencil } from 'tabler-icons-react';
import { showNotification } from '@mantine/notifications';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { Fetch } from '../../services';
import { upperCaseFisrtLetter } from '../../utils';
import { logout } from '../../redux/userReducer';
import { useAppDispatch } from '../../redux/hooks';

type UserAkunType = {
  email: string;
  firstName: string;
  lastName: string;
}

function Akun() {

  const { data } = useQuery(['dashboard-profile'], Fetch.getloggedIn);
  const queryClient = useQueryClient();
  const dispatch = useAppDispatch();
  const [file, setFile] = useState<File | null>(null);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  // const { logout } = useContext(AuthContext);

  const form = useForm({
    initialValues: {
      email: data?.data?.email ?? '',
      firstName: data?.data?.first_name ?? '',
      lastName: data?.data?.last_name ?? '',
    },
    validate: {
      email: (value) => (value ? null : 'email tidak boleh kosong'),
      firstName: (value) => (value ? null : 'nama depan tidak boleh kosong'),
      lastName: (value) => (value ? null : 'nama belakang tidak boleh kosong'),
    },
  });

  const handleLogout = () => {
    dispatch(logout());
  };
  

  const submitProfile = (body: UserAkunType) =>
    Fetch.putProfile({
      email: body.email,
      first_name: body.firstName,
      last_name: body.lastName,
    })

  const submitProfileImage = () => {
    const body = new FormData();
    body.append('file', file ?? '');
    const token = localStorage.getItem('token');
    const headers = {
      'Authorization': token 
    };

    return axios.put(`${import.meta.env.VITE_APP_URL}/profile/image`, body, { headers });
  }

  const mutationUpdateProfile = useMutation(submitProfile, {
    onSuccess: (d: any) => {
      setIsEdit(!isEdit);
      queryClient.invalidateQueries({ queryKey: ['dashboard-profile']});
      showNotification({
        color: 'green',
        message: d.message,
      });
    },
    onError: (err: Error) => {
      showNotification({
        color: 'red',
        message: err.message,
      });
    },
  });

  const mutationUpdateProfileImage = useMutation(submitProfileImage, {
    onSuccess: (d: any) => {
      queryClient.invalidateQueries({ queryKey: ['dashboard-profile']});
      setFile(null);
      showNotification({
        color: 'green',
        message: d.data.message,
      });
    },
    onError: (err: Error) => {
      showNotification({
        color: 'red',
        message: err.message,
      });
    },
  });

  const handleUpdateProfile = (values: any) => {
    mutationUpdateProfile.mutate(values);
  };

  useEffect(() => {
    form.setValues({
      email: data?.data?.email ?? '',
      firstName: data?.data?.first_name ?? '',
      lastName: data?.data?.last_name ?? '',
    })
  }, [data?.data])

  useEffect(() => {
    if (file !== null) {
      mutationUpdateProfileImage.mutate();
    }
  }, [file])

  return (
    <Container my={52} mb={48} size="lg">
      <Grid>
        <Grid.Col>
          <Box
          >
            <Box w="100%">
              <Container maw={500} size="sm">
                <Paper p={30} radius="md">
                  <Stack justify='center'>
                    <Box sx={{ alignSelf: 'center' }}>
                      <Avatar radius="xl" src={file ? URL.createObjectURL(file) : data?.data?.profile_image} h={100} w={100} alt="it's me" />
                      <FileButton onChange={setFile} accept="image/png,image/jpeg">
                        {(props) => <ActionIcon variant="outline" sx={{ borderRadius: '50%', position: 'relative', left: '75px', bottom: '20px', backgroundColor: 'white' }} {...props} ><Pencil size={rem(14)} /></ActionIcon>}
                      </FileButton>
                    </Box>
                    <Text align='center' fw={700}>
                      {upperCaseFisrtLetter(data?.data?.first_name ?? '')}
                      {' '}
                      {upperCaseFisrtLetter(data?.data?.last_name ?? '')}
                    </Text>
                  </Stack>
                  <form onSubmit={form.onSubmit(handleUpdateProfile)}>
                    <TextInput
                      placeholder="masukan email anda"
                      label="Email"
                      disabled
                      icon={<At size="0.8rem" />}
                      required
                      {...form.getInputProps('email')}
                    />
                    <TextInput
                      placeholder="nama depan"
                      label="Nama depan"
                      icon={<User size="0.8rem" />}
                      disabled={!isEdit}
                      required
                      mt="xl"
                      {...form.getInputProps('firstName')}
                    />
                    <TextInput
                      label="Nama belakang"
                      placeholder="nama belakang"
                      icon={<User size="0.8rem" />}
                      disabled={!isEdit}
                      required
                      mt="xl"
                      {...form.getInputProps('lastName')}
                    />
                    {isEdit && (
                      <>
                        <Button
                          fullWidth
                          mt="xl"
                          type="submit"
                          loading={mutationUpdateProfile.isLoading}
                        >
                          Simpan
                        </Button>
                        <Button
                          fullWidth
                          variant='outline'
                          mt="xl"
                          type="submit"
                          onClick={() => setIsEdit(!isEdit)}
                        >
                          Batalkan
                        </Button>
                      </>
                    )}
                  </form>
                  { !isEdit && (
                    <>
                      <Button
                        fullWidth
                        mt="xl"
                        type="submit"
                        onClick={() => setIsEdit(!isEdit)}
                      >
                        Edit Profile
                      </Button>
                      <Button
                        fullWidth
                        variant='outline'
                        mt="xl"
                        type="submit"
                        onClick={() => handleLogout()}
                      >
                        Logout
                      </Button>
                    </>
                  )}
                </Paper>
              </Container>
            </Box>
          </Box>
        </Grid.Col>
      </Grid>
    </Container>
  );
}

export default Akun;
