import {
  Anchor,
  Box,
  Button,
  Container,
  Paper,
  PasswordInput,
  Text,
  TextInput,
  Title,
  Grid,
  Image,
  Group
} from '@mantine/core';
import { showNotification } from '@mantine/notifications';
import { useMutation } from '@tanstack/react-query';
import React from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { At, Lock, User } from 'tabler-icons-react';
import { useForm } from '@mantine/form';
import { Fetch } from '../../services';
import  ilustrationLogin from '../../assets/IllustrasiLogin.png';
import logo from '../../assets/logo.png';

type UserRegisterType = {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
}

function Register() {
  const navigate = useNavigate();
  const form = useForm({
    initialValues: {
      email: '',
      firstName: '',
      lastName: '',
      password: '',
      confirmPassword: '',
    },
    validate: {
      email: (value) => (value ? null : 'email tidak boleh kosong'),
      firstName: (value) => (value ? null : 'nama depan tidak boleh kosong'),
      lastName: (value) => (value ? null : 'nama belakang tidak boleh kosong'),
      ...({
        password: (value) => (value ? null : 'password tidak boleh kosong'),
        confirmPassword: (value, values) =>
          value !== values.password ? 'Passwords tidak sama' : null,
      }),
    },
    
  });

  const submitUser = (body: UserRegisterType) =>
    Fetch.postRegister({
      email: body.email,
      first_name: body.firstName,
      last_name: body.lastName,
      password: body.password,
    })

  const mutationLogin = useMutation(submitUser, {
    onSuccess: (data) => {
      showNotification({
        color: 'green',
        message: data.message,
      });
      setTimeout(() => {
        navigate('/login');
      }, 1000)
    },
    onError: (err: Error) => {
      showNotification({
        color: 'red',
        message: err.message,
      });
    },
  });

  const handleLogin = (values: any) => {
    mutationLogin.mutate(values);
  };  

  if (localStorage.getItem('token')) {
    return <Navigate to="/" />;
  }

  return (
    <Container
      fluid
      h="100vh"
    >
      <Grid>
        <Grid.Col span={6}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
            }}
            h="99vh"
          >
            <Box w="100%">
              <Container maw={500} size="sm">
                <Title
                  align="center"
                  mb={28}
                  sx={(theme) => ({
                    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
                    fontSize: 24,
                    fontWeight: 900,
                    lineHeight: 1.1,
                  })}
                >
                  <Group position='center'>
                    <Image width={25} radius="md" src={logo} alt="Random image" />
                    <span>SIMS PPOB</span>
                  </Group>
                </Title>
                <Title
                  align="center"
                  mb={28}
                  sx={(theme) => ({
                    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
                    fontSize: 28,
                    fontWeight: 900,
                    lineHeight: 1.1,
                  })}
                >
                  Lengkapi data untuk membuat akun
                </Title>
                <Paper p={30} radius="md">
                  <form onSubmit={form.onSubmit(handleLogin)}>
                    <TextInput
                      // name="email"
                      placeholder="masukan email anda"
                      icon={<At size="0.8rem" />}
                      required
                      {...form.getInputProps('email')}
                    />
                    <TextInput
                      // name="firstName"
                      placeholder="nama depan"
                      icon={<User size="0.8rem" />}
                      required
                      mt="xl"
                      {...form.getInputProps('firstName')}
                    />
                    <TextInput
                      // name="lastName"
                      placeholder="nama belakang"
                      icon={<User size="0.8rem" />}
                      required
                      mt="xl"
                      {...form.getInputProps('lastName')}
                    />
                    <PasswordInput
                      // name="password"
                      placeholder="buat paswword"
                      icon={<Lock size="0.8rem" />}
                      required
                      mt="xl"
                      {...form.getInputProps('password')}
                    />
                    <PasswordInput
                      // name="confirmPassword"
                      placeholder="konfirmasi paswword"
                      icon={<Lock size="0.8rem" />}
                      required
                      mt="xl"
                      {...form.getInputProps('confirmPassword')}
                    />
                    <Button
                      fullWidth
                      mt="xl"
                      type="submit"
                      loading={mutationLogin.isLoading}
                    >
                      Masuk
                    </Button>
                  </form>
                </Paper>
              </Container>
              <Text color="dimmed" size="sm" align="center">
                Sudah punya akun? login{' '}
                <Anchor<'a'>
                  href="#"
                  size="sm"
                  color="red"
                  onClick={(event) => {
                    event.preventDefault();
                    navigate('/login');
                  }}
                >
                  di sini
                </Anchor>
              </Text>

            </Box>
          </Box>
        </Grid.Col>
        <Grid.Col span={6}>
          <Box
            sx={{
              backgroundImage: `url(${ilustrationLogin})`,
              height: '100%',
              width: '100%',
            }}
          />
        </Grid.Col>
      </Grid>
    </Container>
  );
}

export default Register;
