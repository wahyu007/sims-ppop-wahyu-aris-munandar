/* eslint-disable no-param-reassign */
import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import type { RootState } from './store';

type UserType = {
  email: string;
  first_name: string;
  last_name: string;
  profile_image: string;
};

type LoginType = {
  token: string;
};

const initialState: UserType = {
  email: '',
  first_name: '',
  last_name: '',
  profile_image: '',
}

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<UserType>) => {
      localStorage.setItem('user', JSON.stringify(action.payload));
      state.email = action.payload.email;
      state.first_name = action.payload.first_name;
      state.last_name = action.payload.last_name;
      state.profile_image = action.payload.profile_image;
    },
    login: (state, action: PayloadAction<LoginType>) => {
      const { token } = action.payload;
      localStorage.setItem('token', `Bearer ${token}`);
    },
    logout: () => {
      localStorage.removeItem('user');
      localStorage.removeItem('token');
      setTimeout(() => {
        window.location.replace('/login');
      }, 500);
    }
  }, 
})

export const currentUser = (state: RootState) => state.user;

export default userSlice.reducer;
export const { setUser, login, logout } = userSlice.actions;
