import { useState } from 'react';

interface UseTableProps {
  pageSize?: number;
}

interface Sort {
  columnAccessor: string;
  direction: 'asc' | 'desc';
}

interface UseTableReturn {
  page: number;
  sort: Sort;
  dates: [Date | null, Date | null];
  search: string;
  status: string;
  pageSize: number;
  column: string,
  onPageChange: (val: number) => void;
  onSortChange: (val: Sort) => void;
  onSearchChange: (val: string) => void;
  onStatusChange: (val: string) => void;
  onColumnChange: (val: string) => void;
  onChangeDates: (val: [Date | null, Date | null])  => void;
}

const INIT_PAGE_SIZE = 10;
const INIT_PAGE = 1;
const INIT_SEARCH = '';
const INIT_COLUMN = '';
const INIT_STATUS = '';
const INIT_SORT: Sort = {
  columnAccessor: '',
  direction: 'asc',
};

const useTable = (props: UseTableProps = {}): UseTableReturn => {
  const [page, setPage] = useState<number>(INIT_PAGE);
  const [sort, setSort] = useState<Sort>(INIT_SORT);
  const [search, setSearch] = useState<string>(INIT_SEARCH);
  const [status, setStatus] = useState<string>(INIT_STATUS);
  const [column, setColumn] = useState<string>(INIT_COLUMN);
  const [dates, setDates] = useState<[Date | null, Date | null]>([new Date(), new Date()]);

  const handlePageChange = (val: number) => {
    setPage(val);
  };

  const handleSortChange = (val: Sort) => {
    setSort(val);
  };

  const handleSearchChange = (val: string) => {
    setSearch(val);
  };

  const handleStatusChange = (val: string) => {
    setStatus(val);
  };

  const handleChangeDates = (val: [Date | null, Date | null]) => {
    setDates(val);
  };

  const handleColumnChange = (val: string) => {
    setColumn(val);
    setSearch('');
  };

  return {
    page,
    sort,
    dates,
    search,
    status,
    column,
    pageSize: props.pageSize || INIT_PAGE_SIZE,
    onChangeDates: handleChangeDates,
    onPageChange: handlePageChange,
    onSortChange: handleSortChange,
    onSearchChange: handleSearchChange,
    onStatusChange: handleStatusChange,
    onColumnChange: handleColumnChange,
  };
};

export default useTable;
