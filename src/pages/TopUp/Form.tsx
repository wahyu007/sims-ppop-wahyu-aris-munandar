/* eslint-disable react/prop-types */
import { Box, Button, Stack, Text, Avatar } from '@mantine/core';
import { closeAllModals } from '@mantine/modals';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { showNotification } from '@mantine/notifications';
import { CircleCheck, CircleX } from 'tabler-icons-react';
import { IApiError } from '../../types';
import logo from '../../assets/logo.png';
import { separateNumber } from '../../utils';

export interface ConfirmContentProps {
  data: any;
  onFetch: (id: number) => any;
  queryKey: (string | number)[];
}

function ConfirmContent({ onFetch, data, queryKey }: ConfirmContentProps) {
  const queryClient = useQueryClient();
  const mutationConfirmContent = useMutation(onFetch, {
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey });
    },
    onError: (err: IApiError) => {
      showNotification({
        color: 'red',
        message: err.message,
      });
    },
  });
  
  const handleConfirmContent = () => mutationConfirmContent.mutate(data);

  return (
    <Box>
      <Stack>
        { mutationConfirmContent.isSuccess && (
          <Avatar sx={{ alignSelf: 'center', height: 50, width: 50 }} alt="no image here" radius="xl" >
            <CircleCheck size={50}  color='green' />
          </Avatar>
        )}
        { mutationConfirmContent.isError && (
          <Avatar sx={{ alignSelf: 'center', height: 50, width: 50 }} alt="no image here" radius="xl">
            <CircleX color='red' size={50} />
          </Avatar>
        )}
        { (!mutationConfirmContent.isError && !mutationConfirmContent.isSuccess) && (
          <Avatar sx={{ alignSelf: 'center', height: 50, width: 50 }} src={logo} alt="no image here" radius="xl" />
        )}
        <Text align='center' size="sm">{(!mutationConfirmContent.isError && !mutationConfirmContent.isSuccess) && 'Anda Yakin untuk '}top up sebesar</Text>
        <Text align='center' size="lg" weight={700}>Rp {separateNumber(data)}</Text>
        { mutationConfirmContent.isSuccess && (
          <Text align='center' size="lg" weight={500}>berhasil</Text>
        )}
        {(!mutationConfirmContent.isError && !mutationConfirmContent.isSuccess) && (
          <Button
            color="red"
            variant="subtle"
            loading={mutationConfirmContent.isLoading}
            onClick={handleConfirmContent}
          >
            Ya, lanjutkan Top Up
          </Button>
        )}
        <Button
          color={(mutationConfirmContent.isError || mutationConfirmContent.isSuccess) ? 'red' :  "dark.0" }
          variant="subtle"
          loading={mutationConfirmContent.isLoading}
          onClick={() => closeAllModals()}
        >
          {(mutationConfirmContent.isError || mutationConfirmContent.isSuccess) ? 'Kembali Ke Beranda' : 'Batalkan'}
        </Button>
      </Stack>
    </Box>
  );
}

export default ConfirmContent;
