import {
  Anchor,
  Box,
  Button,
  Container,
  Paper,
  PasswordInput,
  Text,
  TextInput,
  Title,
  Grid,
  Image,
  Group
} from '@mantine/core';
import { showNotification } from '@mantine/notifications';
import { useMutation } from '@tanstack/react-query';
import React from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { At, Lock } from 'tabler-icons-react';
import { Fetch } from '../../services';
import { IApiError } from '../../types';
import  ilustrationLogin from '../../assets/IllustrasiLogin.png';
import logo from '../../assets/logo.png';
import { login  } from '../../redux/userReducer';
import { useAppDispatch } from '../../redux/hooks';

function Login() {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [form, setForm] = React.useState({
    username: '',
    password: '',
  });

  const mutationLogin = useMutation(Fetch.postLogin, {
    onSuccess: (data) => {
      dispatch(login(data.data));
      navigate('/');
    },
    onError: (err: IApiError) => {
      showNotification({
        color: 'red',
        message: err.message,
      });
    },
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForm((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };

  const handleLogin = async (e: any) => {
    e.preventDefault();
    mutationLogin.mutate(form);
  };

  if (localStorage.getItem('token')) {
    return <Navigate to="/" />;
  }

  return (
    <Container
      fluid
      h="100vh"
    >
      <Grid>
        <Grid.Col span={6}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
            }}
            h="99vh"
          >
            <Box w="100%">
              <Container maw={500} size="sm">
                <Title
                  align="center"
                  mb={28}
                  sx={(theme) => ({
                    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
                    fontSize: 24,
                    fontWeight: 900,
                    lineHeight: 1.1,
                  })}
                >
                  <Group position='center'>
                    <Image width={25} radius="md" src={logo} alt="Random image" />
                    <span>SIMS PPOB</span>
                  </Group>
                </Title>
                <Title
                  align="center"
                  mb={28}
                  sx={(theme) => ({
                    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
                    fontSize: 28,
                    fontWeight: 900,
                    lineHeight: 1.1,
                  })}
                >
                  Masuk atau buat akun untuk memulai
                </Title>
                <Paper p={30} radius="md">
                  <form onSubmit={handleLogin}>
                    <TextInput
                      name="email"
                      placeholder="Masukan email anda"
                      icon={<At size="0.8rem" />}
                      required
                      onChange={handleChange}
                    />
                    <PasswordInput
                      name="password"
                      placeholder="Masukan paswword anda"
                      icon={<Lock size="0.8rem" />}
                      required
                      mt="xl"
                      onChange={handleChange}
                    />
                    <Button
                      fullWidth
                      mt="xl"
                      type="submit"
                      loading={mutationLogin.isLoading}
                    >
                      Masuk
                    </Button>
                  </form>
                </Paper>
              </Container>
              <Text color="dimmed" size="sm" align="center">
                Belum punya akun? registrasi{' '}
                <Anchor<'a'>
                  href="/register"
                  size="sm"
                  color="red"
                >
                  di sini
                </Anchor>
              </Text>

            </Box>
          </Box>
        </Grid.Col>
        <Grid.Col span={6}>
          <Box
            sx={{
              backgroundImage: `url(${ilustrationLogin})`,
              height: '100%',
              width: '100%',
            }}
          />
        </Grid.Col>
      </Grid>
    </Container>
  );
}

export default Login;
