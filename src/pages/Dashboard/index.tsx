/* eslint-disable react/no-unused-prop-types */
import {
  Container,
  Grid,
  Image,
  Text,
  Stack,
  Box,
  SimpleGrid,
  Title,
  Button,
  TextInput,
  Group
} from '@mantine/core';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { Carousel } from '@mantine/carousel';
import { CashBanknote } from 'tabler-icons-react'
import { useState } from 'react';
import { showNotification } from '@mantine/notifications';
import { Fetch } from '../../services';
import { SaldoBox } from '../../components';
import { separateNumber } from '../../utils';

type ServiceType = {
  service_code: string;
  service_name: string;
  service_icon: string;
}

type BannerType = {
  banner_image: string;
  banner_name: string;
  description: string;
}



function Dashboard() {
  const [isPay, setIsPay] = useState<boolean>(false);
  const [payment, setPayment] = useState<ServiceType>()
  const queryServices = useQuery(['dashboard-services'], Fetch.getServices);
  const queryBaners = useQuery(['dashboard-banner'], Fetch.getBanner);
  const queryClient = useQueryClient();

  function handlePayment(p: ServiceType) {
    setIsPay(!isPay);
    setPayment(p)
  }

  const submitPayment = (body: ServiceType | undefined) =>
    Fetch.postPayment({
      service_code: body?.service_code,
    })
  
  const closePayment = () => {
    setIsPay(!isPay);
    setPayment(undefined);
  }
    
  const mutationPay = useMutation(submitPayment, {
    onSuccess: (data) => {
      queryClient.invalidateQueries({ queryKey: ['dashboard-balance'] });
      closePayment();
      showNotification({
        color: 'green',
        message: data.message,
      });
    },
    onError: (err: Error) => {
      showNotification({
        color: 'red',
        message: err.message,
      });
    },
  });

  const pay = () => {
    mutationPay.mutate(payment)
  }

  return (
    <Container my={52} mb={48} size="lg">
      <SaldoBox />
      {  !isPay && (
        <>
          <Grid>
            <Grid.Col>
              <Box mt={30}>
                <SimpleGrid cols={12}>
                  {queryServices.data?.data.map((service: ServiceType) => (
                    <Stack
                      sx={{ '&:hover': { cursor: 'grab', backgroundColor: 'grey', color: 'white' } }}
                      key={service.service_code}
                      spacing={1}
                      onClick={() => handlePayment(service)}
                    >
                      <Image
                        src={service.service_icon}
                        height={50}
                        alt={service.service_name}
                      />
                      <Text size={10} sx={{
                        textWrap: 'wrap',
                        textAlign: 'center',
                        fontWeight: 'normal',
                        marginTop: '12px'
                        
                      }}>
                        {service.service_name}
                      </Text>
                    </Stack>
                  ))}
                </SimpleGrid>
              </Box> 
            </Grid.Col>
          </Grid>
          <Grid>
            <Grid.Col>
              <Box mt={30}>
                <Carousel
                  withIndicators
                  height={200}
                  slideSize="33.333333%"
                  slideGap="md"
                  loop
                  align="start"
                  slidesToScroll={4}
                >
                  {queryBaners.data?.data.map((banner: BannerType) => (
                    <Carousel.Slide key={banner.banner_name}>
                      <Image
                        src={banner.banner_image}
                        alt={banner.banner_name}
                      />
                    </Carousel.Slide>
                  ))}
                </Carousel>
              </Box>
            </Grid.Col>
          </Grid>
        </>
      )}
      { isPay && (
        <>
          <Grid>
            <Grid.Col>
              <Stack mt={30}>
                <Text color="dimmed" size="sm" >
                  Pembayaran
                </Text>
                <Group>
                  <Image
                    src={payment?.service_icon}
                    height={30}
                    width={30}
                    alt={payment?.service_name}
                  />
                  <Title
                    sx={(theme) => ({
                      fontFamily: `Greycliff CF, ${theme.fontFamily}`,
                      fontSize: 20,
                      fontWeight: 900,
                      lineHeight: 1.1,
                    })}
                  >
                    {payment?.service_name}
                  </Title>

                </Group>
              </Stack> 
            </Grid.Col>
          </Grid>
          <Grid>
            <Grid.Col>
              <Stack mt={10}>
                <TextInput disabled icon={ <CashBanknote size={12} />} defaultValue={separateNumber(10000)} />
                <Button onClick={() => pay()}>Bayar</Button>
                <Button variant='outline' onClick={() => closePayment()}>Batalkan</Button>
              </Stack> 
            </Grid.Col>
          </Grid>
        </>
      )}
    </Container>
  );
}

export default Dashboard;
