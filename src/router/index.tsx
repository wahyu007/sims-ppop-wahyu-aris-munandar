import React from 'react';
import {
  BrowserRouter,
  Route,
  Routes,
  Navigate,
  useLocation,
} from 'react-router-dom';
import { Topbar } from '../components';
import { Dashboard, Login, Transaction, Register, TopUp, Akun } from '../pages';

type PrivateRouteType = {
  children: JSX.Element;
};

function PrivateRoute({ children }: PrivateRouteType) {
  const token = localStorage.getItem('token');
  const location = useLocation();

  if (!token) {
    return <Navigate to="/login" state={{ from: location }} replace />;
  }

  return children;
}

function Router() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="login" element={<Login />} />
        <Route path="register" element={<Register />} />
        <Route
          path="/"
          element={
            <PrivateRoute>
              <Topbar />
            </PrivateRoute>
          }
        >
          <Route index element={<Dashboard />} />
          <Route path="/transaksi" element={<Transaction />} />
          <Route path="/topup" element={<TopUp />} />
          <Route path="/akun" element={<Akun />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default Router;
