/* eslint-disable react/no-unused-prop-types */
import {
  Container,
  Grid,
  Stack,
  Title,
  Box,
  Group,
  Text,
  Button
} from '@mantine/core';
import { useQuery } from '@tanstack/react-query';
import { useState } from 'react';
import moment from 'moment';
import { SaldoBox } from '../../components';
import { Fetch } from '../../services';
import { separateNumber } from '../../utils';

type TransactionType = {
  invoice_number: string; 
  transaction_type: 'PAYMENT' | 'TOPUP'; 
  description: string; 
  total_amount: number;
  created_on: string;
}

function Transaction() {

  const [limit, setLimit] = useState<number>(5);
  
  const queryTransaksi = useQuery(['transaksi-all', limit], () => Fetch.getAllHistori({
    offset: 0,
    limit
  }));

  return (
    <Container my={52} mb={48} size="lg">
      <SaldoBox />
      <Grid>
        <Grid.Col>
          <Stack mt={30}>
            <Title
              mb={28}
              sx={(theme) => ({
                fontFamily: `Greycliff CF, ${theme.fontFamily}`,
                fontSize: 20,
                fontWeight: 900,
                lineHeight: 1.1,
              })}
            >
              Semua Transaksi
            </Title>
          </Stack> 
        </Grid.Col>
      </Grid>
      <Grid>
        <Grid.Col span={12}>
          <Stack>
            { queryTransaksi.data?.data?.records?.map((t: TransactionType) => (
              <Box key={t.invoice_number} w="100%" sx={{ border: '0.5px solid #ddd', borderRadius: 10, padding: '10px 20px 10px 20px' }}>
                <Group position='apart'>
                  <Stack>
                    <Text weight={700} size={20} color={ t.transaction_type === 'TOPUP' ? 'green' : 'red'}>
                      {t.transaction_type === 'TOPUP' ? '+' : '-'} Rp.{separateNumber(t.total_amount)}
                    </Text>
                    <Text size={12} color='dark' weight={400}>
                      {moment(t.created_on).format('DD MMMM YYYY HH:mm:ss')}
                    </Text>
                  </Stack>
                  <Text size={14}>
                    {t.description}
                  </Text>
                </Group>
              </Box>
            )) }
          </Stack>
        </Grid.Col>
        <Grid.Col span={12}>
          <Group position='center'>
            <Button variant='subtle' onClick={() => setLimit(limit + 5)}>Show More</Button>
          </Group>
        </Grid.Col>
      </Grid>
    </Container>
  );
}

export default Transaction;
